package com.dokuacademy.messageservice.controllers;

import com.dokuacademy.messageservice.dtos.BaseResponse;
import com.dokuacademy.messageservice.dtos.PesanRequest;
import com.dokuacademy.messageservice.entities.Pesan;
import com.dokuacademy.messageservice.repositories.MessageRepository;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/messages")
public class MessageController {
    private final MessageRepository messageRepository;

    public MessageController(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @GetMapping
    public BaseResponse<List<Pesan>> getAll() {
        List<Pesan> pesans = new ArrayList<>();

        messageRepository.findAll().forEach(pesans::add);
        return new BaseResponse<>("Success", "All pesans", pesans);
    }

    @PostMapping
    public BaseResponse<Pesan> create(@RequestBody PesanRequest pesanRequest) {
//        System.out.println("IN CREATE CONTROLLER");
        UUID uuid = UUID.randomUUID();

        Pesan pesan = new Pesan(uuid.toString(), pesanRequest.getPesan());
//        System.out.println("New Pesan Built");
        Pesan createdPesan = messageRepository.save(pesan);

        return new BaseResponse<>("Success", "Pesan created", createdPesan);
    }
}
