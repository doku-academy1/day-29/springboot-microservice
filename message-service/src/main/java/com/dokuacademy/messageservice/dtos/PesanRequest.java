package com.dokuacademy.messageservice.dtos;

public class PesanRequest {
    public String pesan;

    public PesanRequest() {
    }

    public PesanRequest(String pesan) {
        this.pesan = pesan;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
