package com.dokuacademy.messageservice.repositories;

import com.dokuacademy.messageservice.entities.Pesan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends CrudRepository<Pesan, String> {
}
