package com.dokuacademy.messageservice.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("Pesan-Test")
public class Pesan {
    public String id;
    public String pesan;

    @JsonCreator
    public Pesan() {
    }

    @JsonCreator
    public Pesan(@JsonProperty("id") String id, @JsonProperty("pesan") String pesan) {
        this.id = id;
        this.pesan = pesan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String message) {
        this.pesan = pesan;
    }
}
